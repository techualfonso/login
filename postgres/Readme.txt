Ver las imagenes --> sudo docker images

Borrar imagenes
sudo docker system prune -a

Activar docker de node
sudo docker pull node

Montar la red techu
sudo docker network create redtechu

En la carpeta de postgres...
sudo docker build -t techubank-postgres .


--> Para arrancar postgres
sudo docker run -p 5433:5432 --name postgresql --net redtechu -d techubank-postgres
psql -h localhost -p 5433 -U docker --> pass docker
CREATE DATABASE bdSeguridad;
\connect bdseguridad;

CREATE TABLE usuarios (
	idusuario SERIAL PRIMARY KEY,
	dni CHAR(10) UNIQUE not null,
	password VARCHAR(30) not null,
	nombre VARCHAR(50),
	apellidos VARCHAR(50),
	direccion VARCHAR(50),
	cp CHAR(5),
	provincia VARCHAR(50),
	email VARCHAR(50)
);

ALTER SEQUENCE usuarios_idusuario_seq RESTART WITH 10000;

INSERT INTO usuarios (dni, password, nombre, apellidos, direccion, cp, provincia, email) VALUES ('51982251C', 'TechU2017', 'Luis', 'Ares', 'C/Playa San Juan, 4', '28042', 'Madrid', 'luisaresg@gmail.com');
INSERT INTO usuarios (dni, password, nombre, apellidos, direccion, cp, provincia, email) VALUES ('53006729D', 'TechU2017', 'Alfonso', 'Jimenez', 'C/Tarragona, 66', '28100', 'Madrid', 'alfonsojjg@gmail.com');


En el terminal, en el back
npm install pg --save

npm start


En el front.. polymer serve --open

