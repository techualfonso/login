CREATE TABLE usuarios (
	idusuario SERIAL PRIMARY KEY,
	dni CHAR(10) UNIQUE not null,
	password VARCHAR(30) not null,
	nombre VARCHAR(50),
	apellidos VARCHAR(50),
	direccion VARCHAR(50),
	cp CHAR(5),
	provincia VARCHAR(50),
	email VARCHAR(50)
);

ALTER SEQUENCE usuarios_idusuario_seq RESTART WITH 10000;

INSERT INTO usuarios (dni, password, nombre, apellidos, direccion, cp, provincia, email) VALUES ('51982251C', 'TechU2017', 'Luis', 'Ares', 'C/Playa San Juan, 4', '28042', 'Madrid', 'luisaresg@gmail.com');
INSERT INTO usuarios (dni, password, nombre, apellidos, direccion, cp, provincia, email) VALUES ('53006729D', 'TechU2017', 'Alfonso', 'Jimenez', 'C/Tarragona, 66', '28100', 'Madrid', 'alfonsojjg@gmail.com');
