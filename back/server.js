var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var cors = require('cors');
var app = express();

//variable para poder usar el request-json
var requestJson = require('request-json');
//postgreSQL
var pg= require('pg');
//"postgres://localhost:5433/bdSeguridad";
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad";
//asumo recibo req.body = {usuario:xxxxx, pasword:xxxxx}
var clientePostgre = new pg.Client(urlUsuarios);


//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(cors());

var path = require('path');

// Conexión Mlab
var urlMovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var urlCuentasMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/cuentas";
var apiKey = "apiKey=S26mnsf3dKrF5mRlymz9h6qAF3jkABen";

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.post('/login', function(req, res) {
  clientePostgre.connect();
  console.log("conectado a base de datos postgreSQL");
  console.log("dni: " + req.body.dni + " password: " + req.body.password);
  const query = clientePostgre.query('SELECT idusuario, nombre FROM usuarios WHERE dni =$1 AND password=$2;'
  , [req.body.dni, req.body.password]
  , (err, result) => {
    if(err){
      console.log(err);
      res.send(err);
    }
    else{
      console.log("rowcount es :" + result.rows.length);
      if(result.rows.length==1) {
        console.log("Login correcto idusuario: " + result.rows[0].idusuario);
        console.log("Login correcto nombre: " + result.rows[0].nombre);
        var jsonLogin={};
        jsonLogin.idusuario = result.rows[0].idusuario.toString();
        jsonLogin.nombre = result.rows[0].nombre;
        var respuestaLogin = JSON.stringify(jsonLogin);
        console.log(respuestaLogin);
        res.send(respuestaLogin);
      }
      else{
        console.log("Login incorrecto");
        var jsonLogin={};
        jsonLogin.idusuario = "-1";
        jsonLogin.nombre = "";
        var respuestaLogin = JSON.stringify(jsonLogin);
        console.log(respuestaLogin);
        res.send(respuestaLogin);
      }
    }
  });
});

app.get('/cuentas', function(req, res) {
    console.log("Entro por /cuentas");
    console.log(urlCuentasMlab + "?" + apiKey);
    clienteMlab = requestJson.createClient(urlCuentasMlab + "?" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/cuentas/:idusuario', function(req, res) {
    console.log("Entro por /cuentas/:idusuario");
    console.log(req.params.idusuario);
    console.log(urlCuentasMlab + "?q={'idusuario':"
    + req.params.idusuario + "}" + "&" + apiKey);
    clienteMlab = requestJson.createClient(urlCuentasMlab + "?q={'idusuario':"
    + req.params.idusuario + "}" + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/cuentas/:idusuario/:idcuenta', function(req, res) {
    console.log("Entro por /cuentas/:idusuario/:idcuenta");
    console.log(req.params.idusuario);
    console.log(req.params.idcuenta);
    console.log(urlCuentasMlab + "?q={'idusuario':"
    + req.params.idusuario + ",'idcuenta':" + req.params.idcuenta + "}&" + apiKey);
    clienteMlab = requestJson.createClient(urlCuentasMlab + "?q={'idusuario':"
    + req.params.idusuario + ",'idcuenta':" + req.params.idcuenta + "}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/movimientos', function(req, res) {
    console.log("Entro por /movimientos");
    console.log(urlMovimientosMlab + "?" + apiKey);
    clienteMlab = requestJson.createClient(urlMovimientosMlab + "?" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/movimientos/:idusuario', function(req, res) {
    console.log("Entro por /movimientos/:idusuario");
    console.log(req.params.idusuario);
    console.log(urlMovimientosMlab + "?q={'idusuario':"
    + req.params.idusuario + "}" + "&" + apiKey);
    clienteMlab = requestJson.createClient(urlMovimientosMlab + "?q={'idusuario':"
    + req.params.idusuario + "}" + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/movimientos/:idusuario/:idcuenta', function(req, res) {
    console.log("Entro por /movimientos/:idusuario/:idcuenta");
    console.log(req.params.idusuario);
    console.log(req.params.idcuenta);
    console.log(urlMovimientosMlab + "?q={'idusuario':"
    + req.params.idusuario + ",'idcuenta':" + req.params.idcuenta + "}&" + apiKey);
    clienteMlab = requestJson.createClient(urlMovimientosMlab + "?q={'idusuario':"
    + req.params.idusuario + ",'idcuenta':" + req.params.idcuenta + "}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res){
  console.log("Entro por POST/movimientos");
  console.log(urlMovimientosMlab + "?" + apiKey);
  console.log(req.body);
  clienteMlab = requestJson.createClient(urlMovimientosMlab + "?" + apiKey);
  clienteMlab.post('',req.body, function(err, resM, body){
    if(err)
    {
      console.log(body);
      res.send(400);
    }
    else
    {
      console.log("creado movimiento");
      res.send(200);
    }
  });
});
